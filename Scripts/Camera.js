function Camera( gfx,resolution,focalLength )
{
	this.ctx = gfx.GetContext();
	this.width = gfx.ScreenWidth;
	this.height = gfx.ScreenHeight;
	this.resolution = resolution;
	this.spacing = this.width / resolution;
	this.focalLength = focalLength || 0.8;
	this.range = 24;
	this.lightRange = 18;
	this.scale = ( this.width + this.height ) / 1200;
	// 
	this.Render=( player,map )=>
	{
		this.DrawSky(player.direction, map.skybox, map.light);
		this.DrawColumns(player, map);
		// this.drawWeapon(player.weapon, player.paces);
	}
	this.DrawSky=( direction,sky,ambient )=>
	{
		var width = sky.width * (this.height / sky.height) * 2;
		var left = (direction / ( Math.PI * 2.0 )) * -width;
		
		this.ctx.save();
		this.ctx.drawImage(sky.image, left, 0, width, this.height);
		if (left < width - this.width) {
			this.ctx.drawImage(sky.image, left + width, 0, width, this.height);
		}
		// if (ambient > 0) {
		// 	this.ctx.fillStyle = '#ffffff';
		// 	this.ctx.globalAlpha = ambient * 0.1;
		// 	this.ctx.fillRect(0, this.height * 0.5, this.width, this.height * 0.5);
		// }
		this.ctx.restore();
	}
	this.DrawColumns=( player,map )=>
	{
		this.ctx.save();
		for( var column = 0; column < this.resolution; ++column )
		{
			const x = column / this.resolution - 0.5;
			const angle = Math.atan2(x, this.focalLength);
			const ray = map.Cast(player, player.direction + angle, this.range);
			this.DrawColumn(column, ray, angle, map);
		}
		this.ctx.restore();
	}
	this.DrawColumn=( column,ray,angle,map )=>
	{
		const ctx = this.ctx;
		// const texture = map.wallTexture;
		const left = Math.floor(column * this.spacing);
		const width = Math.ceil(this.spacing);
		let hit = -1;
		
		while( ++hit < ray.length && ray[hit].height <= 0 );
		
		for( var s = ray.length - 1; s >= 0; --s )
		{
			const step = ray[s];
			// var rainDrops = Math.pow(Math.random(), 3) * s;
			// var rain = (rainDrops > 0) && this.Project(0.1, angle, step.distance);
			
			if( s === hit )
			{
				const texture = map.wallTexs[step.height - 1];
				
				if( step.height > 1 ) step.height = 1;
				
				const textureX = Math.floor(texture.width * step.offset);
				const wall = this.Project(step.height, angle, step.distance);
				
				ctx.globalAlpha = 1.0;
				if( left < 0.0 || wall.top < 0.0 || width < 0.0 || wall.height < 0.0 )
				{
					console.log( "X: " + left + " Y: " + wall.top +
						" W: " + width + " H: " + wall.height );
				}
				ctx.drawImage( texture.image,textureX,0,1,texture.height,left,wall.top,width,wall.height );
				
				// ctx.fillStyle = '#000000';
				// ctx.globalAlpha = Math.max((step.distance + step.shading) / this.lightRange - map.light, 0);
				// ctx.fillRect(left, wall.top, width, wall.height);
			}
			
			// ctx.fillStyle = '#ffffff';
			// ctx.globalAlpha = 0.15;
			// while (--rainDrops > 0) ctx.fillRect(left, Math.random() * rain.top, 1, rain.height);
		}
	}
	this.Project=( height,angle,distance )=>
	{
		var z = distance * Math.cos(angle);
		var wallHeight = this.height * height / z;
		var bottom = this.height / 2 * (1 + 1 / z);
		return {
			top: bottom - wallHeight,
			height: wallHeight
		};
	}
}