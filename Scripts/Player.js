function Player(x, y, direction)
{
	this.x = x;
	this.y = y;
	this.direction = direction;
	this.paces = 0;
	// 
	this.Rotate=( angle )=>
	{
		this.direction = (this.direction + angle + ( Math.PI * 2.0 )) % (( Math.PI * 2.0 ));
	}
	this.Walk=( distance,map )=>
	{
		var dx = Math.cos(this.direction) * distance;
		var dy = Math.sin(this.direction) * distance;
		if( map.Get( this.x + dx, this.y ) <= 0 ) this.x += dx;
		if( map.Get( this.x, this.y + dy ) <= 0 ) this.y += dy;
		this.paces += distance;
	}
	this.Update=( kbd,map,seconds )=>
	{
		if (kbd.KeyDown( 'A' )) this.Rotate(-Math.PI * seconds);
		if (kbd.KeyDown( 'D' )) this.Rotate(Math.PI * seconds);
		if (kbd.KeyDown( 'W' )) this.Walk(4 * seconds, map);
		if (kbd.KeyDown( 'S' )) this.Walk(-4 * seconds, map);
	}
}